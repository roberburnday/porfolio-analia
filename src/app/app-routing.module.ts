import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './templates/contact/contact.component';
import { HomeComponent } from './templates/home/home.component';
import { NotFoundComponent } from './templates/not-found/not-found.component';
import { ReferencesComponent } from './templates/references/references.component';

const routes: Routes = [
  //{ path: 'first-component', component: FirstComponent },
    {path:'', component: HomeComponent},
    {path:'references', component: ReferencesComponent},
    {path:'contact', component: ContactComponent},
    {path:'error/404', component: NotFoundComponent},

    //rutas protegidas por api + token (implementar medidas de seguridad en la ruta)

    {path: '**' , redirectTo:'error/404'}, //rutas no especificadas (final)
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

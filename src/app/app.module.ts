import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './templates/home/home.component';
import { ContactComponent } from './templates/contact/contact.component';
import { ReferencesComponent } from './templates/references/references.component';
import { NotFoundComponent } from './templates/not-found/not-found.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { FooterComponent } from './components/footer/footer.component';
import { AboutComponent } from './components/home/about/about.component';
import { LanguagesComponent } from './components/home/languages/languages.component';
import { InterestsComponent } from './components/home/interests/interests.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    ReferencesComponent,
    NotFoundComponent,
    NavigationComponent,
    FooterComponent,
    AboutComponent,
    LanguagesComponent,
    InterestsComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
